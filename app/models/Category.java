package models;

import play.db.jpa.JPA;
import play.db.jpa.Model;
import play.exceptions.UnexpectedException;

import javax.persistence.Entity;
import javax.persistence.PersistenceException;
import javax.persistence.Query;
import java.util.ArrayList;
import java.util.List;

/**
 * Created with IntelliJ IDEA.
 * User: marioosh
 * Date: 09.03.2014
 * Time: 11:38
 */

@Entity
public class Category extends Model {
// ------------------------------ FIELDS ------------------------------

    public int leftValue = 0;

    public int rightValue = 0;

    public int level = 1;

    public Long contextId = null;

    public String name;

// -------------------------- STATIC METHODS --------------------------

    public Category() {

    }

    public Category(String name) {
        this.name = name;
    }

    public static Category createRoot(Category category) {
        return createRoot(category, null);
    }

    public static Category createRoot(Category category, Long contextId) {
        int max = getMaxRight(contextId);
        category.level = 1;
        category.leftValue = max + 1;
        category.rightValue = max + 2;
        category.contextId = contextId;
        category.create();
        return category;
    }

    public static int getMaxRight(Long contextId) {
        Category last = Category.find("level = 1 " + getContextQuery(contextId) + " order by rightValue desc").first();
        if (last == null) {
            return 0;
        } else {
            return last.rightValue;
        }
    }

    public static List<Category> getRoots() {
        return getRoots(null);
    }

    public static List<Category> getRoots(Long contextId) {
        return Category.find("level = ? " + getContextQuery(contextId) + "ORDER BY leftValue ASC", 1).fetch();
    }

    public static boolean isValidTree() {
        return isValidTree(null);
    }

    public static boolean isValidTree(Long contextId) {
        return (getMaxRight(contextId) / 2) == Category.countWithContext(contextId);
    }

    public static long countWithContext(Long contextId) {
        return contextId == null ? count("byContextIdIsNull") : count("byContextId", contextId);
    }

    public static Category findByName(String name, Long contextId) {
        if (contextId == null)
            return Category.find("byNameAndContextIdIsNull", name).first();
        return Category.find("byNameAndContextId", name, contextId).first();
    }

// --------------------------- CONSTRUCTORS ---------------------------

    public static Category findByName(String name) {
        return findByName(name, null);
    }

    /**
     * @param contextId
     * @return " AND contextId [ IS NULL | = ? ]
     */
    private static String getContextQuery(Long contextId) {
        String contextQuery = " AND contextId ";
        contextQuery += contextId == null ? " IS NULL " : " = " + contextId + " ";
        return contextQuery;
    }

// --------------------- GETTER / SETTER METHODS ---------------------

    private String getContextQuery() {
        String contextQuery = " AND contextId ";
        contextQuery += this.contextId == null ? " IS NULL " : " = " + this.contextId + " ";
        return contextQuery;
    }

// -------------------------- OTHER METHODS --------------------------

    public Category addAsFirstSibling(Category subject) {
        int point = 1;
        if (level > 1) {
            point = getParent().getFirstChild().leftValue;
        }

        return createAtPoint(point, level, subject);
    }

    public Category getFirstChild() {
        return Category.find("leftValue > ? AND rightValue < ? " + getContextQuery() + " ORDER BY leftValue ASC", leftValue, rightValue).first();
    }

    private Category createAtPoint(int point, int level, Category subject) {
        if (subject == this) {
            throw new IllegalArgumentException("Cannot add node as related to itself.");
        }
        subject.leftValue = point;
        subject.rightValue = point + 1;
        subject.level = level;
        subject.contextId = this.contextId;
        //shift
        shiftBounds(point, 2);
        subject.create();
        return subject;
    }

    private void shiftBounds(int from, int delta) {
        shiftLeftBounds(from, delta);
        shiftRightBounds(from, delta);
    }

    private void shiftLeftBounds(int from, int delta) {
        shiftLeftBounds(from, from - 1, delta);
    }

    private void shiftRightBounds(int from, int delta) {
        shiftRightBounds(from, from - 1, delta);
    }

    public Category addAsLastSibling(Category subject) {
        int point;
        if (level > 1) {
            point = getParent().getLastChild().rightValue + 1;
        } else if (level == 1) {
            //root level, find maxright
            point = getMaxRight() + 1;
        } else {
            throw new UnsupportedOperationException("level shouldn't be lower than 1");
        }
        return createAtPoint(point, level, subject);
    }

    public Category getLastChild() {
        return Category.find("leftValue > ? AND rightValue < ? " + getContextQuery() + " ORDER BY rightValue DESC", leftValue, rightValue).first();
    }

    public int getMaxRight() {
        return Category.getMaxRight(this.contextId);
    }

    public Category addChild(Category child) {
        return addLastChild(child);
    }

    public Category addLastChild(Category child) {
        return createAtPoint(rightValue, level + 1, child);
    }

    public Category addFirstChild(Category child) {
        return createAtPoint(leftValue + 1, level + 1, child);
    }

    public Category addSiblingAfter(Category subject) {
        return createAtPoint(rightValue + 1, level, subject);
    }

    public Category addSiblingBefore(Category subject) {
        return createAtPoint(leftValue, level, subject);
    }

    /**
     * Delete the entity.
     *
     * @return The deleted entity.
     */
    @Override
    @SuppressWarnings("unchecked")
    public Category delete() {
        List<Category> tree = getDescendants();
        int width = rightValue - leftValue + 1;
        for (Category c : tree) {
            c._delete();
        }
        _delete();
        shiftBounds(rightValue + 1, -width);
        return this;
    }

    public List<Category> getDescendants() {
        return getDescendants(0);
    }

    public List<Category> getAncestorsIncludeSelf() {
        List<Category> categories = getAncestors();
        categories.add(this);
        return categories;
    }

    public List<Category> getAncestors() {
        return Category.find("leftValue < ? AND rightValue > ? " + getContextQuery() + " ORDER BY leftValue ASC", leftValue, rightValue).fetch();
    }

    public List<Category> getChildren() {
        return getDescendants(1);
    }

    public List<Category> getDescendants(int depth) {
        // 0 = unlimited
        StringBuilder query = new StringBuilder();
        query.append("leftValue > ").append(leftValue)
                .append(" AND rightValue < ").append(rightValue);
        if (depth > 0) {
            int maxLevel = level + depth;
            query.append(" AND level > ").append(level)
                    .append(" AND level <= ").append(maxLevel);
        }
        query.append(getContextQuery())
                .append(" ORDER BY leftValue ASC");
        return Category.find(query.toString()).fetch();
    }

    public int getNumberOfDescendants() {
        return (this.rightValue - this.leftValue - 1) / 2;
    }

    public Category getParent() {
        return Category.find("leftValue < ? AND rightValue > ? " + getContextQuery() + " ORDER BY leftValue DESC", leftValue, rightValue).first();
    }

    public boolean hasChildren() {
        return getNumberOfChildren() > 0;
    }

    public int getNumberOfChildren() {
        return getChildren().size();
    }

    public boolean isDescendantOf(Category subject) {
        return getAncestors().contains(subject);
    }

    public boolean isLeaf() {
        return (rightValue - leftValue) == 1;
    }

    public boolean isRoot() {
        return level == 1;
    }

    public boolean isValidNode() {
        return 0 < leftValue && leftValue < rightValue;
    }

    public void moveAsFirstChildOf(Category subject) {
        if (subject == null || subject == this) {
            throw new UnsupportedOperationException("Cannot move as child of itself");
        }
        moveTo(subject.leftValue + 1, subject.level + 1);
    }

    public void moveTo(int newLeft, int newLevel) {
        int width = rightValue - leftValue + 1;

        //check moving is left side or right side
        boolean moveLeft = newLeft < leftValue;

        Node tree = Node.buildFromSource(this);

        if (moveLeft) {
            shiftBounds(newLeft, leftValue, width);
            tree.update(newLeft - leftValue, newLevel - level);
        } else {
            shiftBounds(rightValue + 1, newLeft, -width);
            tree.update(newLeft - leftValue - width, newLevel - level);
        }
    }

    private void shiftBounds(int from, int to, int delta) {
        shiftLeftBounds(from, to, delta);
        shiftRightBounds(from, to, delta);
    }

    /**
     * @param from  leftBoundary inclusive
     * @param to    rightBoundary exclusive
     * @param delta moving ammount
     */
    private void shiftLeftBounds(int from, int to, int delta) {
        String closingBoundQuery = "";
        if (to > from) {
            closingBoundQuery = String.format(" AND leftValue < %d", to);
        }
        Query query = JPA.em().createQuery("UPDATE Category SET leftValue = leftValue + :delta WHERE leftValue >= :from " + closingBoundQuery + getContextQuery());
        int updateCount = query.setParameter("delta", delta).setParameter("from", from).executeUpdate();
    }

    /**
     * @param from  leftBoundary inclusive
     * @param to    rightBoundary exclusive
     * @param delta moving ammount
     */
    private void shiftRightBounds(int from, int to, int delta) {
        String closingBoundQuery = "";
        if (to > from) {
            closingBoundQuery = String.format(" AND rightValue < %d", to);
        }
        Query query = JPA.em().createQuery("UPDATE Category SET rightValue = rightValue + :delta WHERE rightValue >= :from " + closingBoundQuery + getContextQuery());
        int updateCount = query.setParameter("delta", delta).setParameter("from", from).executeUpdate();
    }

    public void moveAsLastChildOf(Category subject) {
        if (subject == null || subject == this) {
            throw new UnsupportedOperationException("Cannot move as child of itself");
        }
        moveTo(subject.rightValue, subject.level + 1);
    }

    public void moveAsNextSiblingOf(Category subject) {
        if (subject == null || subject == this) {
            throw new UnsupportedOperationException("Cannot move as sibling of itself");
        }
        moveTo(subject.rightValue + 1, subject.level);
    }

    public void moveAsPreviousSiblingOf(Category subject) {
        if (subject == null || subject == this) {
            throw new UnsupportedOperationException("Cannot move as sibling of itself");
        }
        moveTo(subject.leftValue, subject.level);
    }

    public void moveLeft() {
        //root
        if (level == 1 && leftValue == 1) {
            throw new UnsupportedOperationException("Cannot move left first element");
        }
        //first child
        if (level > 1 && getParent().getFirstChild().equals(this)) {
            throw new UnsupportedOperationException("Cannot move left first element");
        }

        //find category to position
        Category leftCat = Category.find("rightValue = ?", leftValue - 1).first();
        if (leftCat == null) throw new UnexpectedException("moving left to nowhere");

        moveTo(leftCat.leftValue, leftCat.level);
    }

    public void moveRight() {
        if (level == 1 && rightValue == getMaxRight()) {
            throw new UnsupportedOperationException("Cannot move right first element");
        }
        //last child
        if (level > 1 && getParent().getLastChild().equals(this)) {
            throw new UnsupportedOperationException("Cannot move left last element");
        }
        //find right side category
        Category rightCat = Category.find("leftValue = ?", rightValue + 1).first();
        if (rightCat == null) throw new UnexpectedException("Moving right to nowhere");

        moveTo(rightCat.rightValue + 1, rightCat.level);
    }

    @Override
    @SuppressWarnings("unchecked")
    public Category save() {
        if (isPersistent())
            return super.save();
        throw new PersistenceException("For creating NestedSet Model use 'createRoot' or 'addChild' methods.");
    }

// -------------------------- INNER CLASSES --------------------------

    static class Node {
        private Category category;

        private Node parent;

        private int left = 0;

        private int right = 0;

        private int subjectLevel = 0;

        private List<Node> children = new ArrayList<Node>();

        public static Node buildFromSource(Category category) {
            Node node = new Node();
            node.setCategory(category);
            node.setLeft(category.leftValue);
            node.setRight(category.rightValue);
            node.setParent(null);
            node.subjectLevel = category.level;

            for (Category c : category.getChildren()) {
                Node nc = Node.buildFromSource(c);
                nc.setParent(node);
                node.getChildren().add(nc);
            }
            return node;
        }

        public static void printTree(Node node) {
            node.printTree();
        }

        public Category getCategory() {
            return category;
        }

        public void setCategory(Category category) {
            this.category = category;
        }

        public Node getParent() {
            return parent;
        }

        public void setParent(Node parent) {
            this.parent = parent;
        }

        public int getNodeLevel() {
            if (getParent() == null) {
                return 0;
            } else {
                return 1 + getParent().getNodeLevel();
            }
        }

        public int getLevel() {
            return subjectLevel;
        }

        public void setLevel(int subjectLevel) {
            this.subjectLevel = subjectLevel;
        }

        public int getLeft() {
            return left;
        }

        public void setLeft(int left) {
            this.left = left;
        }

        public int getRight() {
            return right;
        }

        public void setRight(int right) {
            this.right = right;
        }

        public List<Node> getChildren() {
            return children;
        }

        public void setChildren(List<Node> children) {
            this.children = children;
        }

        public void printTree() {
            System.out.println(toRow());
            for (Node n : getChildren()) {
                n.printTree();
            }
        }

        public String toRow() {
            StringBuilder sb = new StringBuilder();
            for (int i = 0; i < getNodeLevel(); i++) {
                sb.append("--");
            }

            sb.append(String.format("[%d]-|%s|-[%d]", getLeft(), getCategory().name, getRight()));
            return sb.toString();
        }

        public String toString() {
            return String.format("[%d<->%d][%d|%d]%s", left, right, getNodeLevel(), getLevel(), category.name);
        }

        public void update(int delta, int level) {
//            category.refresh();
            category.leftValue = getLeft() + delta;
            category.rightValue = getRight() + delta;

            category.level = category.level + level;
            category.save();
            for (Node n : children) {
                n.update(delta, level);
            }
        }
    }
}
