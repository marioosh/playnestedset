package models;

import org.junit.Before;
import org.junit.Test;
import play.test.Fixtures;
import play.test.UnitTest;

import javax.persistence.PersistenceException;
import java.util.Arrays;
import java.util.List;

/**
 * Created with IntelliJ IDEA.
 * User: marioosh
 * Date: 09.03.2014
 * Time: 12:14
 */
public class CategoryTest extends UnitTest {
// -------------------------- OTHER METHODS --------------------------

    @Test(expected = IllegalArgumentException.class)
    public void addAfterSelf() {
        loadBasicTree();
        Category lcd = Category.findByName("LCD", 187l);
        assertNotNull(lcd);
        lcd = lcd.addSiblingAfter(lcd);
    }

    @Test
    public void addAsFirstSibling() {
        loadBasicTree();
        Category electronics = Category.findByName("Electronics", 187l);
//        printTree(electronics);
        //

        Category lcd = Category.findByName("LCD", 187l);
        assertNotNull(lcd);

        Category usb = new Category("USB TV");
        usb = lcd.addAsFirstSibling(usb);
        assertNotNull(usb);
        lcd.refresh();

        Category tv = Category.findByName("Televisions", 187l);
        assertNotNull(tv.refresh());
        electronics.refresh();

        assertNotNull(electronics);
        //assertions
//        printTree(electronics);

        assertEquals(3, usb.leftValue);
        assertEquals(4, usb.rightValue);
        assertEquals(3, usb.level);
        assertEquals(new Long(187), usb.contextId);

        assertEquals(7, lcd.leftValue);
        assertEquals(8, lcd.rightValue);

        assertEquals(11, tv.rightValue);

        assertEquals(usb, tv.getChildren().get(0));
        assertEquals(4, tv.getNumberOfChildren());
    }

    @Test
    public void addAsFirstSiblingInRoot() {
        loadBasicTree();
        Category electronics = Category.findByName("Electronics", 187l);
        assertNotNull(electronics);

        Category clothes = new Category("Clothes");
        clothes = electronics.addAsFirstSibling(clothes);
        electronics.refresh();

        assertNotNull(clothes);
        assertNotNull(electronics);
        //
        assertNull(clothes.getParent());
        assertEquals(1, clothes.level);
        assertEquals(1, clothes.leftValue);
        assertEquals(2, clothes.rightValue);
        assertEquals(new Long(187), clothes.contextId);

        assertEquals(1, electronics.level);
        assertEquals(3, electronics.leftValue);
        assertEquals(22, electronics.rightValue);
    }

    @Test(expected = IllegalArgumentException.class)
    public void addAsFirstSiblingofSelf() {
        loadBasicTree();
        Category lcd = Category.findByName("LCD", 187l);
        assertNotNull(lcd);
        lcd = lcd.addAsFirstSibling(lcd);
    }

    @Test
    public void addAsLastSibling() {
        loadBasicTree();
        Category electronics = Category.findByName("Electronics", 187l);
        Category lcd = Category.findByName("LCD", 187l);
        Category tv = Category.findByName("Televisions", 187l);
        assertNotNull(tv);
        assertNotNull(lcd);
        assertNotNull(electronics);

        Category usb = new Category("USB TV");
        usb = lcd.addAsLastSibling(usb);
        assertNotNull(usb);

        lcd.refresh();
        tv.refresh();
        electronics.refresh();

        //assertions
        assertEquals(11, tv.rightValue);
        assertEquals(4, tv.getNumberOfChildren());

        assertEquals(9, usb.leftValue);
        assertEquals(10, usb.rightValue);
        assertEquals(3, usb.level);
        assertEquals(new Long(187), usb.contextId);

        assertEquals(22, electronics.rightValue);
    }

    @Test
    public void addAsLastSiblingInRoot() {
        loadBasicTree();
        Category electronics = Category.findByName("Electronics", 187l);
        assertNotNull(electronics);
        Category clothes = new Category("Clothes");
        clothes = electronics.addAsLastSibling(clothes);
        electronics.refresh();

        assertNotNull(clothes);
        assertNotNull(electronics);
        //
        assertNull(clothes.getParent());
        assertEquals(1, clothes.level);
        assertEquals(21, clothes.leftValue);
        assertEquals(22, clothes.rightValue);
        assertEquals(new Long(187), clothes.contextId);

        assertEquals(1, electronics.level);
        assertEquals(1, electronics.leftValue);
        assertEquals(20, electronics.rightValue);
    }

    @Test(expected = IllegalArgumentException.class)
    public void addAsLastSiblingSelf() {
        loadBasicTree();
        Category lcd = Category.findByName("LCD", 187l);
        assertNotNull(lcd);
        lcd = lcd.addAsLastSibling(lcd);
    }

    @Test(expected = IllegalArgumentException.class)
    public void addBeforeSelf() {
        loadBasicTree();
        Category lcd = Category.findByName("LCD", 187l);
        assertNotNull(lcd);
        lcd = lcd.addSiblingBefore(lcd);
    }

    @Test(expected = IllegalArgumentException.class)
    public void addFirstChildSelf() {
        loadBasicTree();
        Category tv = Category.findByName("Televisions", 187l);
        assertNotNull(tv);
        tv = tv.addFirstChild(tv);
    }

    @Test
    public void addFirstChildTest() {
        loadBasicTree();
        Category portable = Category.findByName("Portable Electronics", 187l);
        assertNotNull(portable);

        Category mp4 = new Category("MP4");
        mp4 = portable.addFirstChild(mp4);

        portable.refresh();

        //assertions
        assertEquals(4, portable.getNumberOfChildren());
        assertEquals(5, portable.getNumberOfDescendants());

        assertEquals(11, mp4.leftValue);
        assertEquals(12, mp4.rightValue);
        assertEquals(3, mp4.level);
        assertEquals(new Long(187), mp4.contextId);

        assertEquals(10, portable.leftValue);
        assertEquals(21, portable.rightValue);

        Category flash = Category.findByName("Flash", 187l);
        assertNotNull(flash);

        assertEquals(14, flash.leftValue);
        assertEquals(15, flash.rightValue);
    }

    @Test(expected = IllegalArgumentException.class)
    public void addLastChildSelf() {
        loadBasicTree();
        Category tv = Category.findByName("Televisions", 187l);
        assertNotNull(tv);
        tv = tv.addChild(tv);
    }

    @Test
    public void addLastChildTest() {
        loadBasicTree();

        Category tv = Category.findByName("Televisions", 187l);
        assertNotNull(tv);

        Category usb = new Category("USB TV");
        usb = tv.addLastChild(usb);

        tv.refresh();
        //asertions

        assertEquals(4, tv.getNumberOfChildren());
        assertEquals(11, tv.rightValue);
        assertEquals(9, usb.leftValue);
        assertEquals(10, usb.rightValue);
        assertEquals(3, usb.level);
        assertEquals(new Long(187), usb.contextId);

        Category electronics = Category.findByName("Electronics", 187l);
        assertNotNull(electronics);
        assertEquals(1, electronics.leftValue);
        assertEquals(22, electronics.rightValue);
    }

    @Test
    public void addSiblingAfter() {
        loadBasicTree();

        Category lcd = Category.findByName("LCD", 187l);
        assertNotNull(lcd);

        Category usb = new Category("USB TV");
        usb = lcd.addSiblingAfter(usb);
        assertNotNull(usb);
        lcd.refresh();

        Category tv = Category.findByName("Televisions", 187l);
        assertNotNull(tv);
        //assertions
        assertEquals(7, usb.leftValue);
        assertEquals(8, usb.rightValue);
        assertEquals(3, usb.level);
        assertEquals(new Long(187), usb.contextId);
        assertEquals(tv, usb.getParent());
        assertEquals(4, tv.getNumberOfChildren());
        assertEquals(11, tv.rightValue);
    }

    @Test
    public void addSiblingBefore() {
        loadBasicTree();

        Category lcd = Category.findByName("LCD", 187l);
        assertNotNull(lcd);

        Category usb = new Category("USB TV");
        usb = lcd.addSiblingBefore(usb);
        assertNotNull(usb);
        lcd.refresh();

        Category tv = Category.findByName("Televisions", 187l);
        assertNotNull(tv);

        //assertions
        assertEquals(5, usb.leftValue);
        assertEquals(6, usb.rightValue);
        assertEquals(7, lcd.leftValue);
        assertEquals(8, lcd.rightValue);

        assertEquals(11, tv.rightValue);
        assertEquals(3, usb.level);
        assertEquals(new Long(187), usb.contextId);
        assertEquals(4, tv.getNumberOfChildren());
        assertEquals(4, tv.getNumberOfDescendants());

        assertTrue(usb.isLeaf());
        assertFalse(usb.isRoot());
    }

    @Test
    public void buildNodes() {
        loadBasicTree();
        Category electronics = Category.findByName("Electronics", 187l);
        assertNotNull(electronics);
        Category.Node root = Category.Node.buildFromSource(electronics);
        root.printTree();
    }

    @Test
    public void createRootTest() {
        long counter = Category.count();

        Category cat = new Category("Programming");

        cat = Category.createRoot(cat);

        assertEquals(counter + 1, Category.count());
        assertEquals(1, cat.leftValue);
        assertEquals(2, cat.rightValue);
        assertEquals(1, cat.level);

        //find saved
        Category cat1 = Category.findByName("Programming");
        assertNotNull(cat1);
        assertEquals(1, cat1.leftValue);
        assertEquals(2, cat1.rightValue);
        assertEquals(1, cat1.level);

        Category cat2 = new Category("Java");
        cat2 = Category.createRoot(cat2);

        cat2 = Category.findByName("Java");
        assertNotNull(cat2);
        assertEquals(counter + 2, Category.count());
        //verify model
        assertEquals(3, cat2.leftValue);
        assertEquals(4, cat2.rightValue);
        assertEquals(1, cat2.level);
    }


    @Test
    public void findRoots() {
        loadBasicTree();
        Category agd = new Category("AGD");

        agd = Category.createRoot(agd, 187l);

        List<Category> roots = Category.getRoots(187l);
        assertNotNull(roots);
        assertFalse(roots.isEmpty());
        assertEquals(2, roots.size());

        List<String> rootNames = Arrays.asList("AGD", "Electronics");
        for (Category c : roots) {
            assertTrue(rootNames.contains(c.name));
        }
        Category pcr = new Category("PCR");
        pcr = Category.createRoot(pcr);
        List<Category> rootsWO = Category.getRoots();
        assertNotNull(rootsWO);
        assertFalse(rootsWO.isEmpty());
        assertEquals(1, rootsWO.size());
    }

    @Test
    public void getAncesors() {
        loadBasicTree();

        Category mp3 = Category.findByName("MP3 Players", 187l);
        assertNotNull(mp3);
        List<Category> ancestors = mp3.getAncestors();
        assertNotNull(ancestors);
        assertFalse(ancestors.isEmpty());
        assertEquals(2, ancestors.size());
        assertEquals("Electronics", ancestors.get(0).name);
        assertEquals("Portable Electronics", ancestors.get(1).name);
    }

    @Test
    public void getAncesorsIncludeSelf() {
        loadBasicTree();

        Category mp3 = Category.findByName("MP3 Players", 187l);
        assertNotNull(mp3);
        List<Category> ancestors = mp3.getAncestorsIncludeSelf();
        assertNotNull(ancestors);
        assertFalse(ancestors.isEmpty());
        assertEquals(3, ancestors.size());
        assertEquals("Electronics", ancestors.get(0).name);
        assertEquals("Portable Electronics", ancestors.get(1).name);
        assertEquals("MP3 Players", ancestors.get(2).name);
    }

    @Test
    public void getFirstLastChild() {
        loadBasicTree();

        Category portable = Category.findByName("Portable Electronics", 187l);
        assertNotNull(portable);
        Category first = portable.getFirstChild();
        assertNotNull(first);
        assertEquals("MP3 Players", first.name);

        Category last = portable.getLastChild();
        assertNotNull(last);
        assertEquals("Radio", last.name);
    }

    @Test
    public void getNumberOfDescendants() {
        loadBasicTree();

        Category electronics = Category.findByName("Electronics", 187l);
        assertNotNull(electronics);
        assertEquals(2, electronics.getNumberOfChildren());
        assertEquals(9, electronics.getNumberOfDescendants());
        assertTrue(electronics.hasChildren());
        assertTrue(electronics.isRoot());


        Category portable = Category.findByName("Portable Electronics", 187l);
        assertNotNull(portable);
        assertEquals(3, portable.getNumberOfChildren());
        assertEquals(4, portable.getNumberOfDescendants());
        assertFalse(portable.isLeaf());

        Category mp3 = Category.findByName("MP3 Players", 187l);
        assertNotNull(mp3);
        assertEquals(1, mp3.getNumberOfChildren());
        assertEquals(1, mp3.getNumberOfDescendants());

        Category lcd = Category.findByName("LCD", 187l);
        assertNotNull(lcd);
        assertEquals(0, lcd.getNumberOfChildren());
        assertEquals(0, lcd.getNumberOfDescendants());
        assertFalse(lcd.hasChildren());
        assertTrue(lcd.isLeaf());
        assertFalse(lcd.isRoot());
    }

    @Test
    public void getParent() {
        loadBasicTree();

        Category flash = Category.findByName("Flash", 187L);
        assertNotNull(flash);
        assertNotNull(flash.getParent());
        assertEquals("MP3 Players", flash.getParent().name);
    }

    @Test
    public void isDescendantOf() {
        loadBasicTree();

        Category flash = Category.findByName("Flash", 187l);
        assertNotNull(flash);

        Category portable = Category.findByName("Portable Electronics", 187l);
        assertNotNull(portable);

        Category tv = Category.findByName("Televisions", 187l);
        assertNotNull(tv);

        assertTrue(flash.isDescendantOf(portable));
        assertFalse(flash.isDescendantOf(tv));
    }

    @Test
    public void isValidNode() {
        Category cat = new Category("name");
        assertFalse(cat.isValidNode());
        cat.leftValue = 2;
        assertFalse(cat.isValidNode());
        cat.rightValue = 1;
        assertFalse(cat.isValidNode());
        cat.rightValue = 3;
        assertTrue(cat.isValidNode());
    }

    @Test
    public void loadFixtures() {
        loadBasicTree();

        assertEquals(10, Category.count());
        Category lcd = Category.findByName("LCD", 187l);
        assertNotNull(lcd);
        assertEquals(3, lcd.level);
        assertEquals(5, lcd.leftValue);
        assertEquals(6, lcd.rightValue);

        Category.isValidTree();
    }

    private void loadBasicTree() {
        Fixtures.loadModels("categories.yml");
    }

    @Test(expected = UnsupportedOperationException.class)
    public void moveLeftFirstChild() {
        loadBasicTree();
        Category tube = Category.findByName("Tube", 187l);
        assertNotNull(tube);
        tube.moveLeft();
    }

    @Test(expected = UnsupportedOperationException.class)
    public void moveLeftFirstRoot() {
        loadBasicTree();
        Category electronics = Category.findByName("Electronics", 187l);
        assertNotNull(electronics);
        electronics.moveLeft();
    }

    @Test(expected = UnsupportedOperationException.class)
    public void moveRightLastChild() {
        loadBasicTree();
        Category radio = Category.findByName("Radio", 187l);
        assertNotNull(radio);
        radio.moveRight();
    }

    @Test(expected = UnsupportedOperationException.class)
    public void moveRightRoot() {
        loadBasicTree();
        Category electronics = Category.findByName("Electronics", 187l);
        assertNotNull(electronics);
        electronics.moveRight();
    }

    @Test(expected = PersistenceException.class)
    public void safeSaveTest() {
        Category cat = new Category("Programowanie");
        cat.save();
    }

    @Test
    public void safeUpdate() {
        long counter = Category.count();
        Category cat = new Category("rogramowanie");
        cat = Category.createRoot(cat);
        assertEquals(counter + 1, Category.count());
        cat = Category.findByName("rogramowanie");
        assertNotNull(cat);
        cat.name = "Programowanie";
        cat = cat.save();

        assertEquals(counter + 1, Category.count());
        cat = Category.findByName("Programowanie");
        assertNotNull(cat);
    }

    private void loadBigTree() {
        Fixtures.loadModels("categories-bigtree.yml");
    }

    private void loadSmallTree() {
        Fixtures.loadModels("categories-context.yml");
        Fixtures.loadModels("categories-small.yml");
    }

    @Test
    public void loadBigTreeTest() {
        loadBigTree();
        assertEquals(106, Category.getMaxRight(null));
        assertEquals(53, Category.count());

        Category food = Category.findByName("Food");
        assertNotNull(food);
        assertTrue(food.isValidNode());
        assertTrue(Category.isValidTree());
        Category.Node tree = Category.Node.buildFromSource(food);

//        tree.printTree();

        assertTrue(checkCategory("MP3", 4, 90, 103));
        assertTrue(checkCategory("Kivi", 4, 20, 21));
        assertTrue(checkCategory("MB2C", 5, 39, 40));
        assertTrue(checkCategory("MB3CA", 6, 48, 49));
        assertTrue(checkCategory("MP2BA", 6, 84, 85));
        assertTrue(checkCategory("MP1BBA", 7, 69, 70));
        assertTrue(checkCategory("MP3CA", 6, 98, 99));

    }

    @Test
    public void loadSmallTreeTest() {
        loadSmallTree();
        assertEquals(22, Category.getMaxRight(null));
        assertEquals(11, Category.countWithContext(null));
        assertEquals(12, Category.countWithContext(123L));
        assertTrue(checkCategory("A1", 1, 1, 6));
        assertTrue(checkCategory("A11", 2, 2, 3));

        assertTrue(checkCategory("B12", 2, 10, 13));
        assertTrue(checkCategory("B121", 3, 11, 12));
        assertTrue(checkCategory("C11", 2, 16, 19));
        assertTrue(checkCategory("C111", 3, 17, 18));
        assertTrue(checkCategory("D1", 1, 21, 22));
        //tree with context
        assertTrue(checkCategory("A121", 3, 5, 6, 123l));
        assertTrue(checkCategory("A1", 1, 1, 8, 123l));
        assertTrue(checkCategory("C1", 1, 15, 16, 123l));
        assertTrue(checkCategory("D111", 3, 19, 20, 123l));
        assertTrue(checkCategory("E1", 1, 23, 24, 123l));
    }

    @Test
    public void moveLeftValidBigTree() {
        loadBigTree();
        Category green = Category.findByName("Green");
        assertNotNull(green);
        green.moveLeft();

        //parent not changed
        assertTrue(checkCategory("Fruit", 2, 2, 25));

        //left side moved to right
        assertTrue(checkCategory("Yellow", 3, 17, 22));
        assertTrue(checkCategory("Banana", 4, 18, 19));
        assertTrue(checkCategory("Citron", 4, 20, 21));

        //moved on position
        assertTrue(checkCategory("Green", 3, 9, 16));

        assertTrue(checkCategory("Green Apple", 4, 10, 11));
        assertTrue(checkCategory("Grape", 4, 12, 13));
        assertTrue(checkCategory("Kivi", 4, 14, 15));

    }

    @Test
    public void moveRightValidBigTree() {
        loadBigTree();
        Category yellow = Category.findByName("Yellow");
        assertNotNull(yellow);

        yellow.moveRight();

        //parent not changed
        assertTrue(checkCategory("Fruit", 2, 2, 25));

        //moved on position
        assertTrue(checkCategory("Yellow", 3, 17, 22));
        assertTrue(checkCategory("Banana", 4, 18, 19));
        assertTrue(checkCategory("Citron", 4, 20, 21));

        //moved right side to left
        assertTrue(checkCategory("Green", 3, 9, 16));

        assertTrue(checkCategory("Green Apple", 4, 10, 11));
        assertTrue(checkCategory("Grape", 4, 12, 13));
        assertTrue(checkCategory("Kivi", 4, 14, 15));


    }

    private boolean checkCategory(String categoryName, int lvl, int lft, int rgt, Long contextId) {
        Category cat = Category.findByName(categoryName, contextId);
        cat.refresh();
        return cat != null && cat.level == lvl && cat.leftValue == lft && cat.rightValue == rgt;
    }

    private boolean checkCategory(String categoryName, int lvl, int lft, int rgt) {
        return checkCategory(categoryName, lvl, lft, rgt, null);
    }


    @Before
    public void setup() {
        Fixtures.delete(Category.class);
    }


    @Test
    public void smallTreeMove1() {
        loadSmallTree();
        Category a1 = Category.findByName("A1");
        assertNotNull(a1);
        a1.moveTo(15, 1);
        //
        assertTrue(checkCategory("B1", 1, 1, 8));
        assertTrue(checkCategory("B12", 2, 4, 7));
        assertTrue(checkCategory("A1", 1, 9, 14));
        assertTrue(checkCategory("C1", 1, 15, 20));
        //tree with context
        assertTrue(checkCategory("A121", 3, 5, 6, 123l));
        assertTrue(checkCategory("A1", 1, 1, 8, 123l));
        assertTrue(checkCategory("C1", 1, 15, 16, 123l));
        assertTrue(checkCategory("D111", 3, 19, 20, 123l));
        assertTrue(checkCategory("E1", 1, 23, 24, 123l));
    }

    @Test
    public void smallTreeMove2() {
        loadSmallTree();
        Category b1 = Category.findByName("B1");
        assertNotNull(b1);
        b1.moveTo(1, 1);
        //
        assertTrue(checkCategory("B1", 1, 1, 8));
        assertTrue(checkCategory("B12", 2, 4, 7));
        assertTrue(checkCategory("A1", 1, 9, 14));
        assertTrue(checkCategory("C1", 1, 15, 20));
        //tree with context
        assertTrue(checkCategory("A121", 3, 5, 6, 123l));
        assertTrue(checkCategory("A1", 1, 1, 8, 123l));
        assertTrue(checkCategory("C1", 1, 15, 16, 123l));
        assertTrue(checkCategory("D111", 3, 19, 20, 123l));
        assertTrue(checkCategory("E1", 1, 23, 24, 123l));
    }

    @Test
    public void smallTreeMove3() {
        loadSmallTree();
        Category a12 = Category.findByName("A12");
        assertNotNull(a12);
        a12.moveTo(10, 2);
        //
        assertTrue(checkCategory("A1", 1, 1, 4));
        assertTrue(checkCategory("B1", 1, 5, 14));
        assertTrue(checkCategory("B11", 2, 6, 7));
        assertTrue(checkCategory("B12", 2, 10, 13));
        assertTrue(checkCategory("A12", 2, 8, 9));
        //tree with context
        assertTrue(checkCategory("A121", 3, 5, 6, 123l));
        assertTrue(checkCategory("A1", 1, 1, 8, 123l));
        assertTrue(checkCategory("C1", 1, 15, 16, 123l));
        assertTrue(checkCategory("D111", 3, 19, 20, 123l));
        assertTrue(checkCategory("E1", 1, 23, 24, 123l));
    }

    @Test
    public void smallTreeMove4() {
        loadSmallTree();
        Category c11 = Category.findByName("C11");
        assertNotNull(c11);
        c11.moveTo(11, 3);
        //
        assertTrue(checkCategory("C1", 1, 19, 20));
        assertTrue(checkCategory("B1", 1, 7, 18));
        assertTrue(checkCategory("B12", 2, 10, 17));
        assertTrue(checkCategory("C11", 3, 11, 14));
        assertTrue(checkCategory("C111", 4, 12, 13));
        assertTrue(checkCategory("B121", 3, 15, 16));
        //tree with context
        assertTrue(checkCategory("A121", 3, 5, 6, 123l));
        assertTrue(checkCategory("A1", 1, 1, 8, 123l));
        assertTrue(checkCategory("C1", 1, 15, 16, 123l));
        assertTrue(checkCategory("D111", 3, 19, 20, 123l));
        assertTrue(checkCategory("E1", 1, 23, 24, 123l));
    }

    @Test(expected = UnsupportedOperationException.class)
    public void moveAsFirstChildTestSelf() {
        loadSmallTree();
        Category c = Category.findByName("C1");
        assertNotNull(c);
        //
        c.moveAsFirstChildOf(c);

    }

    @Test(expected = UnsupportedOperationException.class)
    public void moveAsLastChildTestSelf() {
        loadSmallTree();
        Category c = Category.findByName("C1");
        assertNotNull(c);
        //
        c.moveAsLastChildOf(c);

    }

    @Test(expected = UnsupportedOperationException.class)
    public void moveAsNextSiblingOfSelf() {
        loadSmallTree();
        Category c = Category.findByName("C1");
        assertNotNull(c);
        //
        c.moveAsNextSiblingOf(c);

    }

    @Test(expected = UnsupportedOperationException.class)
    public void moveAsNextPreviuosOfSelf() {
        loadSmallTree();
        Category c = Category.findByName("C1");
        assertNotNull(c);
        //
        c.moveAsPreviousSiblingOf(c);

    }

    @Test
    public void moveAsFirstChildTest() {
        loadSmallTree();
        Category b1 = Category.findByName("B1");
        Category c1 = Category.findByName("C1");
        assertNotNull(b1);
        b1.moveAsFirstChildOf(c1);
        //
        assertTrue(checkCategory("C1", 1, 7, 20));
        assertTrue(checkCategory("B1", 2, 8, 15));
        assertTrue(checkCategory("C11", 2, 16, 19));
        assertTrue(checkCategory("C111", 3, 17, 18));

        assertTrue(checkCategory("B11", 3, 9, 10));
        assertTrue(checkCategory("B12", 3, 11, 14));
        assertTrue(checkCategory("B121", 4, 12, 13));

        //tree with context
        assertTrue(checkCategory("A121", 3, 5, 6, 123l));
        assertTrue(checkCategory("A1", 1, 1, 8, 123l));
        assertTrue(checkCategory("C1", 1, 15, 16, 123l));
        assertTrue(checkCategory("D111", 3, 19, 20, 123l));
        assertTrue(checkCategory("E1", 1, 23, 24, 123l));
    }

    @Test
    public void moveAsLastChildTest() {
        loadSmallTree();
        Category c1 = Category.findByName("C1");
        Category b1 = Category.findByName("B1");
        assertNotNull(b1);
        assertNotNull(c1);

        c1.moveAsLastChildOf(b1);
        //
        assertTrue(checkCategory("B1", 1, 7, 20));
        assertTrue(checkCategory("C1", 2, 14, 19));
        assertTrue(checkCategory("B12", 2, 10, 13));
        assertTrue(checkCategory("C11", 3, 15, 18));
        assertTrue(checkCategory("C111", 4, 16, 17));
        //tree with context
        assertTrue(checkCategory("A121", 3, 5, 6, 123l));
        assertTrue(checkCategory("A1", 1, 1, 8, 123l));
        assertTrue(checkCategory("C1", 1, 15, 16, 123l));
        assertTrue(checkCategory("D111", 3, 19, 20, 123l));
        assertTrue(checkCategory("E1", 1, 23, 24, 123l));
    }

    @Test
    public void moveAsNextSiblingTest() {
        loadSmallTree();
        Category c1 = Category.findByName("C1");
        Category b12 = Category.findByName("B12");
        assertNotNull(b12);
        assertNotNull(c1);

        c1.moveAsNextSiblingOf(b12);
        //
        assertTrue(checkCategory("B1", 1, 7, 20));
        assertTrue(checkCategory("C1", 2, 14, 19));
        assertTrue(checkCategory("B12", 2, 10, 13));
        assertTrue(checkCategory("C11", 3, 15, 18));
        assertTrue(checkCategory("C111", 4, 16, 17));
        //tree with context
        assertTrue(checkCategory("A121", 3, 5, 6, 123l));
        assertTrue(checkCategory("A1", 1, 1, 8, 123l));
        assertTrue(checkCategory("C1", 1, 15, 16, 123l));
        assertTrue(checkCategory("D111", 3, 19, 20, 123l));
        assertTrue(checkCategory("E1", 1, 23, 24, 123l));
    }

    @Test
    public void moveAsPreviousSiblingTest() {
        loadSmallTree();
        Category c11 = Category.findByName("C11");
        Category b1 = Category.findByName("B1");
        assertNotNull(b1);
        assertNotNull(c11);

        b1.moveAsPreviousSiblingOf(c11);
        //
        assertTrue(checkCategory("C1", 1, 7, 20));
        assertTrue(checkCategory("B1", 2, 8, 15));
        assertTrue(checkCategory("C11", 2, 16, 19));
        assertTrue(checkCategory("C111", 3, 17, 18));

        assertTrue(checkCategory("B11", 3, 9, 10));
        assertTrue(checkCategory("B12", 3, 11, 14));
        assertTrue(checkCategory("B121", 4, 12, 13));
        //tree with context
        assertTrue(checkCategory("A121", 3, 5, 6, 123l));
        assertTrue(checkCategory("A1", 1, 1, 8, 123l));
        assertTrue(checkCategory("C1", 1, 15, 16, 123l));
        assertTrue(checkCategory("D111", 3, 19, 20, 123l));
        assertTrue(checkCategory("E1", 1, 23, 24, 123l));
    }

    @Test
    public void delete1() {
        loadSmallTree();
        Category b12 = Category.findByName("B12");
        assertNotNull(b12);
        b12.delete();
        //
        assertTrue(checkCategory("B1", 1, 7, 10));
        assertTrue(checkCategory("C1", 1, 11, 16));
        assertTrue(Category.isValidTree());
        //tree with context
        assertTrue(checkCategory("A121", 3, 5, 6, 123l));
        assertTrue(checkCategory("A1", 1, 1, 8, 123l));
        assertTrue(checkCategory("C1", 1, 15, 16, 123l));
        assertTrue(checkCategory("D111", 3, 19, 20, 123l));
        assertTrue(checkCategory("E1", 1, 23, 24, 123l));
    }


    @Test
    public void delete2() {
        loadSmallTree();
        Category d1 = Category.findByName("D1");
        d1.delete();
        //
        assertEquals(20, Category.getMaxRight(null));
        assertTrue(Category.isValidTree());
        //tree with context
        assertTrue(checkCategory("A121", 3, 5, 6, 123l));
        assertTrue(checkCategory("A1", 1, 1, 8, 123l));
        assertTrue(checkCategory("C1", 1, 15, 16, 123l));
        assertTrue(checkCategory("D111", 3, 19, 20, 123l));
        assertTrue(checkCategory("E1", 1, 23, 24, 123l));
    }

    @Test
    public void delete3() {
        loadSmallTree();
        Category c1 = Category.findByName("C1");
        c1.delete();
        assertTrue(checkCategory("B1", 1, 7, 14));
        assertTrue(checkCategory("D1", 1, 15, 16));
        assertTrue(Category.isValidTree());
        //tree with context
        assertTrue(checkCategory("A121", 3, 5, 6, 123l));
        assertTrue(checkCategory("A1", 1, 1, 8, 123l));
        assertTrue(checkCategory("C1", 1, 15, 16, 123l));
        assertTrue(checkCategory("D111", 3, 19, 20, 123l));
        assertTrue(checkCategory("E1", 1, 23, 24, 123l));
    }

    //CONTEXT Tests
    @Test
    public void createRootTestWithContext() {
        long counter = Category.count();

        Category cat = new Category("Programming");

        cat = Category.createRoot(cat);  //wo context

        assertEquals(counter + 1, Category.count());
        assertEquals(1, cat.leftValue);
        assertEquals(2, cat.rightValue);
        assertEquals(1, cat.level);

        //find saved
        Category cat1 = Category.findByName("Programming");
        assertNotNull(cat1);
        assertEquals(1, cat1.leftValue);
        assertEquals(2, cat1.rightValue);
        assertEquals(1, cat1.level);

        Category cat2 = new Category("Java");
        cat2 = Category.createRoot(cat2);

        cat2 = Category.findByName("Java");
        assertNotNull(cat2);
        assertEquals(counter + 2, Category.count());
        //verify model
        assertEquals(3, cat2.leftValue);
        assertEquals(4, cat2.rightValue);
        assertEquals(1, cat2.level);

        Category cat3 = new Category("food");
        cat3 = Category.createRoot(cat3, 123L);


        cat3 = Category.findByName("food", 123L);
        assertNotNull(cat3);
        assertEquals(counter + 3, Category.count());
        assertEquals(1, cat3.leftValue);
        assertEquals(2, cat3.rightValue);
        assertEquals(1, cat3.level);
        assertEquals(new Long(123), cat3.contextId);

        //
        assertEquals(2, Category.count("byContextIdIsNull"));
        assertEquals(1, Category.count("byContextId", 123L));

    }

}
